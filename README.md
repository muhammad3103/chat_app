# Project ChatWeb

## Description
This project is a web-based chat application with WebSocket implementation. It was undertaken as a challenge presented by my internship mentor. The goal is to create a real-time chat experience using modern web technologies.

## Features
- **Login**: Users can securely log in to the chat application.
- **Register**: New users can create an account to join the chat community.
- **View Chat Contacts**: Users can see a list of friends or contacts available for chat.
- **Real-time Chat**: The application provides a seamless real-time chat experience.

## Technologies Used

### Frontend
- **Next.js**
- **Shadcn UI**
- **Zod Validation Form**
- **Tanstack React Query**
- **Socket.io**
- **TypeScript**

### Backend
- **Express.js**
- **MySQL**
- **Socket.io**
- **TypeScript**

## Video Demo
Check out the [video demonstration](https://youtu.be/gTOnRjvFvwk) to get a visual overview of the project.
